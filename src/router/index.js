import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    component: () =>
      import(/* webpackChunkName: "login" */ "../views/Redirect.vue"),
  },
  {
    path: "/login",
    name: "login",
    component: () =>
      import(/* webpackChunkName: "login" */ "../views/auth/Login.vue"),
    meta: {
      guest: true,
    },
  },
  {
    path: "/register",
    name: "register",
    component: () =>
      import(/* webpackChunkName: "register" */ "../views/auth/Register.vue"),
    meta: {
      guest: true,
    },
  },
  {
    path: "/forgot-password",
    name: "forgot-password",
    component: () =>
      import(
        /* webpackChunkName: "forgot-password" */ "../views/auth/ForgotPassword.vue"
      ),
    meta: {
      guest: true,
    },
  },

  {
    path: "/dashboard",
    name: "dashboard",
    component: () =>
      import(/* webpackChunkName: "dashboard" */ "../views/Dashboard.vue"),
    children: [
      {
        path: "",
        redirect: "my-apps",
      },
  
      {
        path: "users",
        name: "users",
        component: () =>
          import(
            /* webpackChunkName: "my-groups" */ "../views/dashboard/users/Index.vue"
          ),
      },
      {
        path: "images",
        name: "images",
        component: () =>
          import(
            /* webpackChunkName: "my-groups" */ "../views/dashboard/users/Images.vue"
          ),
      },
      {
        path: "users-details/:id",
        name: "users-details",
        component: () =>
          import(
            /* webpackChunkName: "my-groups" */ "../views/dashboard/users/View.vue"
          ),
      },
      {
        path: "create-user",
        name: "create-user",
        component: () =>
          import(
            /* webpackChunkName: "my-groups" */ "../views/dashboard/users/Create.vue"
          ),
      },
      {
        path: "users/:id",
        name: "users.show",
        component: () =>
          import(
            /* webpackChunkName: "groups.show" */ "../views/dashboard/users/Show.vue"
          ),
      },
  
      {
        path: "my-apps",
        name: "my-apps",
        component: () =>
          import(
            /* webpackChunkName: "my-apps" */ "../views/dashboard/main/Index.vue"
          ),
      },
    ],
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  scrollBehavior() {
    return { x: 0, y: 0 };
  },
  routes,
});
export default router;
