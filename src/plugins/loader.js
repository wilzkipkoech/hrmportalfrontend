export default {
  // eslint-disable-next-line no-unused-vars
  install(Vue, options) {
    Vue.loader = {
      start(full = false, type = "default") {
        if (type == "default") {
          let loader = document.createElement("div");
          loader.id = "app-loader";
          loader.classList.add("page-loader");

          if (full) {
            loader.classList.add("full");
          }

          let loader_element = document.createElement("div");
          loader_element.classList.add("spinner-border");

          let loader_span = document.createElement("span");
          loader_span.innerHTML = "Loading";

          loader.append(loader_element);
          loader.append(loader_span);

          document.body.append(loader);
        } else if (type == "simulator") {
          let loader = document.createElement("div");
          loader.id = "simulator-loader";

          let loader_element = document.createElement("div");
          loader_element.classList.add("app-loading");

          let loader_span = document.createElement("span");
          loader_span.innerHTML = "Loading";

          loader.append(loader_element);
          loader.append(loader_span);

          document.getElementById("app-simulator").append(loader);
        }
      },

      finish() {
        try {
          document.getElementById("app-loader").remove();
        } catch (error) {}
        try {
          document.getElementById("simulator-loader").remove();
        } catch (error) {}
      }
    };

    Object.defineProperties(Vue.prototype, {
      $loader: {
        get() {
          return Vue.loader;
        }
      }
    });
  }
};
