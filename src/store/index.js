import Vue from 'vue'
import Vuex from 'vuex'
import User from './users'
import App from './app'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    User, App
  }
})
