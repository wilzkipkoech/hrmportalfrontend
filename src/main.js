import Vue from "vue";
import "./plugins/axios";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./styles";
import "./scripts";
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";

import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

// Install BootstrapVue
Vue.use(BootstrapVue);
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin);

Vue.config.productionTip = false;

import Loader from "./plugins/loader";
Vue.use(Loader);

import Auth from "./plugins/auth";
Vue.use(Auth);

import Modal from "./components/Modal";
Vue.component("modal", Modal);

const app = new Vue({
  router,
  store,
  vuetify: new Vuetify(),
  render: (h) => h(App),
}).$mount("#app");

// axios request interceptors
app.$axios.interceptors.request.use(
  function(config) {
    let loader_full = app.$route.meta.guest ? true : false;
    let loader_type = app.$route.meta.simulator ? "simulator" : "default";

    app.$loader.start(loader_full, loader_type);

    return config;
  },
  function(error) {
    app.$loader.finish();
    // Do something with request error
    return Promise.reject(error);
  }
);

// Add a response interceptor
app.$axios.interceptors.response.use(
  function(response) {
    app.$loader.finish();

    return response;
  },
  function(error) {
    app.$loader.finish();

    return Promise.reject(error);
  }
);
